FROM alpine:3.20.0

RUN apk add --update \
    tor \
    && rm -rf /var/cache/apk/*

WORKDIR /var/lib/tor

CMD cp -L /root/torrc.sample /etc/tor/ && \
    chmod u+w /etc/tor/torrc.sample && \
    mkdir -p /var/lib/tor/metager_hidden_v3 && \
    cp -L /root/metager_hidden_v3/hostname /var/lib/tor/metager_hidden_v3/ && \
    cp -L /root/metager_hidden_v3/hs_ed25519_secret_key /var/lib/tor/metager_hidden_v3/ && \
    cp -L /root/metager_hidden_v3/hs_ed25519_public_key /var/lib/tor/metager_hidden_v3/ && \
    chown -R 100:65533 /var/lib/tor/metager_hidden_v3 && \
    chmod -R u=+rwX,g=,o= /var/lib/tor/metager_hidden_v3 && \
    su -s /bin/sh -c 'tor -f /etc/tor/torrc.sample' tor
